# 测试基座文档

## 设计架构

测试基座分为5个主要组件，每个组件都会编译为一个同名bin可执行文件，同时main主程序也会被编译成主程序入口main。进行测试时，用户启动main程序，main会依次调用source、CDN、sche、daemon、download完成下载流程，并记录相关下载信息。

| 组件         | 描述                                                         |
| ------------ | ------------------------------------------------------------ |
| source       | 启动本地文件服务器作为下载源，并初始化被下载文件             |
| mod_CDN      | 根据配置在本地启动1个dragonfly CDN                           |
| mod_sche     | 根据配置在本地启动1个dragonfly scheduler                     |
| mod_daemon   | 根据配置在本地启动多个dragonfly daemon                       |
| mod_download | 根据配置启动多个下载流序列，并记录下载耗时                   |
| main         | 测试启动入口，读取测试配置，完成测试流程，及环境初始化和清理 |
| utils        | 定义测试用的各种路径及工具函数                               |
| deploy_sches | 自定义scheduler，可以被非侵入式注入dragonfly                 |

## 同步思路

不同程序之间通过写入特定文件和检测文件是否存在进行同步

```sequence
Note left of source: 启动下载源
Note right of source: 下载源启动完成
source->sche: 写入文件A
sche->source: 文件A存在

Note left of sche: 启动scheduler
Note right of sche: scheduler启动完成
sche->CDN: 写入文件B
CDN->sche: 文件B存在

Note left of CDN: 启动CDN
Note right of CDN: CDN启动完成
CDN->daemon: 写入文件C
daemon->CDN: 文件C存在

Note left of daemon: 启动daemon
Note right of daemon: daemon启动完成
daemon->download: 写入文件D
download->daemon: 文件D存在

Note left of download: 启动下载
Note right of download: 下载完成

download->source: 写入文件E
source->download: 文件E存在
Note left of source: 停止所有服务器
```

## 配置方案

标准化配置文件

```yaml
# 预定义可下载文件序列及每个文件大小
files:
- [5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000]
# 下载源服务器地址和端口，-1表示自动获得空闲端口
source:
  ip: 127.0.0.1
  port: -1
# CDN的RPC服务和文件服务器地址和端口，-1表示自动获得空闲端口
cdn:
  ip: 127.0.0.1
  listen-port: -1
  down-port: -1
# scheduler地址和端口，-1表示自动获得空闲端口
scheduler:
  ip: 127.0.0.1
  port: -1
# 启动的多个daemon的地址和端口，-1表示自动获得空闲端口
daemons:
- ip: 127.0.0.1
  rpc-port: -1
  up-port: -1
- ip: 127.0.0.1
  rpc-port: -1
  up-port: -1
- ip: 127.0.0.1
  rpc-port: -1
  up-port: -1
# 下载序列，每个序列代表一个模拟客户端，会依次执行序列中的文件下载，不同序列之间会并行执行
gets:
- [0,1,2,3,4,5,6,7]
- [0,1,2,3,4,5,6,7]
- [0,1,2,3,4,5,6,7]
```

## 数据收集

不同程序的输出和错误会被重定向到 `.out`和 `.err`文件中，在 `download.out`中，目前已经实现了对下载时间的统计，具体如下：

```
# 序列4下载文件7耗时
seq no 4: download file no 7 success, length: 5000, Byte cost: 4ms
# 序列4下载总耗时
seq no 4 finished in 2090ms
# 所有序列下载的最小值/最大值/平均值/中位值
seq finish finished time:
min: 170ms max: 2090ms mean: 310ms median: 220ms
# 所有序列下载的总耗时
all seqs finished time 2092ms
```

## 运行说明

编译和运行命令

```
mkdir tmp
cd tmp
../build.sh
../bin/main
```

## 备注

对于库supermonkey， 需要将 `[fix: Fix panic when calling PatchGuard.Restore by Mutated1994 · Pull Request #17 · cch123/supermonkey (github.com)](https://github.com/cch123/supermonkey/pull/17)`中的修改拷贝到源码中，保证 `Restore`不会造成Panic。

对于库dragonfly，需要将 `client/daemon/daemon.go`中的一句注释掉，配置的不同Socket地址才能生效，保证daemon之间不出现冲突

```go
func (cd *clientDaemon) Serve() error {
	cd.GCManager.Start()
	// TODO remove this field, and use directly dfpath.DaemonSockPath
	// cd.Option.Download.DownloadGRPC.UnixListen.Socket = dfpath.DaemonSockPath
```

## Q&A

### 为什么不在一个程序中完成所有服务器的创建？

scheduler和daemon都需要调用grpc，在导入grpc包时会定义某个全局变量，但这个变量不允许第二次被定义。因此只能分开用多个可执行程序，启动各个服务器。

### 为什么放弃了hook函数的方案？

之前的思路是，对每个daemon执行时，存在

上传核心函数 `d7y.io/dragonfly/v2/client/daemon/upload.(*uploadManager).handleUpload`

和下载核心函数 `d7y.io/dragonfly/v2/client/daemon/peer.(*pieceDownloader).DownloadPiece`

对这两个函数的每一次调用，通过在调用前后进行插桩，可以从参数中获得 `peerID`、`taskID`、`length`信息，再通过在调用前后嵌入计时代码，可以获得daemon的每一次上传下载信息。因此，这里需要一个能够进行mock/hook的工具，去调研了golang中可以使用的hook框架。

| 框架        | 重写导出函数 | 重写非导出函数 | 非侵入性             | 函数中调用原函数 | 调用非导出函数 |
| ----------- | ------------ | -------------- | -------------------- | ---------------- | -------------- |
| gomock      | 支持         | 支持           | 否，需要嵌入mock接口 | 支持             | 无             |
| gomonkey    | 支持         | 不支持         | 是                   | 支持             | 无             |
| supermonkey | 支持         | 支持           | 是                   | 支持             | 无             |

最终确认supermonkey可以重写非导出函数，且为非侵入性hook，但是**所有框架**都无法在重写函数中调用**原非导出函数**。找到了一个实现类似功能的库go-forceexport，但是在当前的golang版本 `1.16`下无法工作。

除此之外，并非完全放弃了hook的方案，对于scheduler中的函数 `Scheduler.SchedulerChildren`和 `Scheduler.Schedulerparent`都是导出函数，后续可以用hook的方式去输出scheduler的规划细节。

### 测试框架有没有通用性？

因为现在不再hook非导出函数，所以说框架具有较高的通用性，在dragonfly的导出API不修改的情况下，可以继续使用。如何测试不同scheduler

### 如何导入自定义不同调度器

定义在 `deploy_sche`中，并在 `mod_sche/hack.go`中导入自定义scheduler。在导入过程中自动实现 `register`，实际选择scheduler通过配置文件 `sche.yaml`指定，可以指定多个不同测试环境和不同调度器的交叉测试

### 不同scheduler和数据对比显示

完成，目前可以指定多个不同的scheduler和env，并在表格中对比输出运行时间

```
2021/08/18 15:48:31 load 2 test cases
2021/08/18 15:48:31 begin workflow for sche basic and env normal
2021/08/18 15:48:51 begin workflow for sche dynamic and env normal
2021/08/18 15:49:11 begin workflow for sche basic and env simple
2021/08/18 15:49:31 begin workflow for sche dynamic and env simple
+-------------------------+-----------------+-----------------+
| MIN/MEDIAN/MEAN/MAX(MS) |      BASIC      |     DYNAMIC     |
+-------------------------+-----------------+-----------------+
| normal                  | 129/242/227/281 | 137/251/236/261 |
| simple                  | 94/94/94/96     | 102/104/116/144 |
+-------------------------+-----------------+-----------------+

```
