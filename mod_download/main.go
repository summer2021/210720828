package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"path"
	"time"

	"d7y.io/dragonfly/v2/pkg/basic"
	"d7y.io/dragonfly/v2/pkg/basic/dfnet"
	"d7y.io/dragonfly/v2/pkg/rpc/base"
	"d7y.io/dragonfly/v2/pkg/rpc/dfdaemon"
	"d7y.io/dragonfly/v2/pkg/rpc/dfdaemon/client"

	"multiply/utils"
)

var finish chan int = make(chan int)
var seqTime map[int]int64

func InitialDownload(daemon dfnet.NetAddr, downloadAddr string, files []int, index int) {
	seqTime = make(map[int]int64)
	storage := path.Join(utils.DownloadDir, fmt.Sprint(index))
	err := os.MkdirAll(storage, 0775)
	if err != nil {
		log.Panicf("make dir error: %s", err)
	}
	daemonsAdr := []dfnet.NetAddr{daemon}
	seqStart := time.Now()
	for _, file := range files {
		daemonClient, err := client.GetClientByAddr(daemonsAdr)
		if err != nil {
			log.Panicf("connect target error: %s", err)
		}

		hdr := make(map[string]string)
		url := fmt.Sprintf("http://%s/%d", downloadAddr, file)
		out := path.Join(storage, fmt.Sprintf("%d", file))
		request := &dfdaemon.DownRequest{
			Url:               url,
			Output:            out,
			Timeout:           0,
			Limit:             float64(20 * 1024 * 1024),
			DisableBackSource: true,
			UrlMeta: &base.UrlMeta{
				Digest: "",
				Tag:    "",
				Range:  "",
				Filter: "",
				Header: hdr,
			},
			Pattern:    "",
			Callsystem: "",
			Uid:        int64(basic.UserID),
			Gid:        int64(basic.UserGroup),
		}
		var (
			start     = time.Now()
			result    *dfdaemon.DownResult
			downError error
		)
		stream, err := daemonClient.Download(context.Background(), request)

		// begin download
		for {
			if result, downError = stream.Recv(); downError != nil {
				break
			}
			// success
			if result.Done {
				fmt.Printf("seq no %d: download file no %d success, length: %d, Byte cost: %dms\n", index, file, result.CompletedLength, time.Now().Sub(start).Milliseconds())
				break
			}
		}
	}
	seqTime[index] = time.Now().Sub(seqStart).Milliseconds()
	fmt.Printf("seq no %d finished in %dms\n", index, seqTime[index])
	finish <- 1
}

func main() {
	// 读取配置
	configFile := path.Join(utils.ConfigDir, "config.yaml")
	cfg := utils.MultiplyConfig{}
	utils.LoadConfig(configFile, &cfg)
	// 启动钩子
	InitialHook()

	// 并行启动下载
	var daemons []dfnet.NetAddr
	for _, daemonCfg := range cfg.Daemons {
		daemonAddr := fmt.Sprintf("%s:%d", daemonCfg.IP, daemonCfg.RPCPort)
		daemons = append(daemons, dfnet.NetAddr{Type: dfnet.TCP, Addr: daemonAddr})
	}
	downloadAddr := fmt.Sprintf("%s:%d", cfg.Source.IP, cfg.Source.Port)

	start := time.Now()
	for i, files := range cfg.GetsFileSeq {
		go InitialDownload(daemons[i], downloadAddr, files, i)
	}
	// 等待下载都完成
	for range cfg.GetsFileSeq {
		<-finish
	}

	intervals := time.Now().Sub(start).Milliseconds()

	var seqList []int
	for _, v := range seqTime {
		seqList = append(seqList, int(v))
	}
	sta := utils.GetStatus(seqList)
	utils.SaveStatus(sta)
	fmt.Printf("seq finish finished time:\nmin: %dms max: %dms mean: %dms median: %dms\n",
		sta.MinScheWork, sta.MaxScheWork, sta.MeanScheWork, sta.MeanScheWork)
	fmt.Printf("all seqs finished time %dms\n", intervals)
	// 检验文件存在
	for i, files := range cfg.GetsFileSeq {
		storageDirNow := path.Join(utils.DownloadDir, fmt.Sprint(i))
		for _, file := range files {
			dst := path.Join(storageDirNow, fmt.Sprint(file))
			if _, err := os.Stat(dst); os.IsNotExist(err) {
				log.Printf("download file not exist: %s", err)
				panic(err)
			}
		}
	}
	utils.WriteForFinish("download", 0)
}
