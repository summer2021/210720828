module multiply

go 1.16

replace d7y.io/dragonfly/v2 => ./import/Dragonfly2

require (
	d7y.io/dragonfly/v2 v2.0.0-alpha-inner-test
	github.com/cch123/supermonkey v1.0.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/otiai10/copy v1.6.0
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	gopkg.in/yaml.v2 v2.4.0
)
