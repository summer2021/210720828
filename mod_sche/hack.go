package main

// 增加新scheduler要在这里加import
import (
	"d7y.io/dragonfly/v2/scheduler/config"
	"d7y.io/dragonfly/v2/scheduler/core"

	_ "multiply/deploy_sches/scheduler/dynamic"
	_ "multiply/deploy_sches/scheduler/exchange"
	_ "multiply/deploy_sches/scheduler/exchangeMin"

	_ "d7y.io/dragonfly/v2/scheduler/core/scheduler/basic"
	sm "github.com/cch123/supermonkey"
)

func InitialHook(sche string) {
	// 自动选择scheduler注入dragonfly
	var patchGuard *sm.PatchGuard
	patchGuard = sm.Patch(core.NewSchedulerService, func(cfg *config.SchedulerConfig, dynConfig config.DynconfigInterface, openTel bool) (*core.SchedulerService, error) {
		patchGuard.Unpatch()
		defer patchGuard.Restore()

		cfg.Scheduler = sche
		service, err := core.NewSchedulerService(cfg, dynConfig, openTel)
		return service, err
	})
}
