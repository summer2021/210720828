package main

import (
	"fmt"
	"log"
	"path"

	"d7y.io/dragonfly/v2/scheduler"
	"d7y.io/dragonfly/v2/scheduler/config"

	"multiply/utils"
)

func InitialScheduler(scheOpt utils.ServerOptionMultiply) (*scheduler.Server, string) {
	cfg := config.New()
	err := cfg.Validate()
	if err != nil {
		log.Panicf("sche config error: %s", err)
	}
	cfg.Server.IP, cfg.Server.Port = scheOpt.IP, scheOpt.Port
	cfg.DynConfig.CDNDirPath = path.Join(utils.ConfigDir, "cdn")

	// create server
	sche, err := scheduler.New(cfg)
	log.Printf("init scheduler:%s:%d", scheOpt.IP, scheOpt.Port)
	if err != nil {
		log.Panicf("create server error: %s", err)
	}

	// start server
	go sche.Serve()
	log.Printf("start scheduler:%s:%d", scheOpt.IP, scheOpt.Port)
	// forbidden auto choose port
	return sche, fmt.Sprintf("%s:%d", scheOpt.IP, scheOpt.Port)
}

func main() {
	// 读取配置
	configFile := path.Join(utils.ConfigDir, "config.yaml")
	cfg := utils.MultiplyConfig{}
	utils.LoadConfig(configFile, &cfg)

	// 启动钩子
	InitialHook(cfg.Scheduler.Name)
	// 创建scheduler
	InitialScheduler(cfg.Scheduler)

	utils.WriteForFinish("sche", 3)
	log.Printf("write seq file sche")

	utils.WaitForFinish("download")
	log.Printf("server closed")
}
