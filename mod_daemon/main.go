package main

import (
	"flag"
	"fmt"
	"log"
	"path"

	"d7y.io/dragonfly/v2/client/config"
	"d7y.io/dragonfly/v2/client/daemon"
	"d7y.io/dragonfly/v2/pkg/basic/dfnet"

	"multiply/utils"
)

func InitialDaemon(dataDir string, clientOpt utils.DaemonOptionMultiply, scheAddr string, index int) {

	opt := config.NewDaemonConfig()

	opt.Host.ListenIP = clientOpt.IP
	opt.Upload.TCPListen.PortRange.Start = clientOpt.UpPort

	opt.Download.PeerGRPC.TCPListen.PortRange.Start = clientOpt.RPCPort
	opt.Download.DownloadGRPC.UnixListen.Socket = path.Join(utils.DaemonDir, fmt.Sprintf("daemon%d.sock", index))
	target := dfnet.NetAddr{Type: dfnet.TCP, Addr: scheAddr}
	opt.Scheduler.NetAddrs = []dfnet.NetAddr{target}
	opt.DataDir = dataDir
	opt.WorkHome = dataDir
	opt.Storage.DataPath = dataDir

	dae, err := daemon.New(opt)
	if err != nil {
		log.Panicf("connect target error: %s", err)
	}
	go dae.Serve()
	return
}

var index = flag.Int("index", -1, "Index of daemon")

func main() {
	flag.Parse()

	// 读取配置
	configFile := path.Join(utils.ConfigDir, "config.yaml")
	cfg := utils.MultiplyConfig{}
	utils.LoadConfig(configFile, &cfg)
	if *index < 0 || *index >= len(cfg.Daemons) {
		log.Panicln("bad index")
	}
	// 启动钩子
	InitialHook()
	// 并行创建daemon
	scheAddr := fmt.Sprintf("%s:%d", cfg.Scheduler.IP, cfg.Scheduler.Port)

	clientCfg := cfg.Daemons[*index]
	daemonDirNow := path.Join(utils.DaemonDir, fmt.Sprint(*index))
	InitialDaemon(daemonDirNow, clientCfg, scheAddr, *index)
	log.Println(fmt.Sprintf("daemon %d up", *index))

	utils.WriteForFinish(fmt.Sprintf("daemon_%d", *index), 3)
	log.Printf("write seq file daemon")

	utils.WaitForFinish("download")
	log.Printf("server closed")
}
