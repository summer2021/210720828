package exchange

import (
	"sort"

	"d7y.io/dragonfly/v2/scheduler/supervisor"
)

type DiffOfChild struct {
	peer        *supervisor.Peer
	diff        int32
	finishedNum int32
}

// 从大到小的有序数组
// 最好支持自动去重
// 支持迭代，二分插入
// 操作：插入新节点，如果节点已存在查出来过，更新，执行多
// 操作：从前到后迭代，要求已经是排好序的，执行少
// 用map + bucket sort
type ExchangeInfo struct {
	derivation map[*supervisor.Peer]DiffOfChild
}

// piece = 4M-15M
// nc = 100
func bucketSort(dd []*DiffOfChild, nc int) []*DiffOfChild {
	// bucket sort diff -2*nc to 2*nc -> 0 to 4*nc
	// normal sort inner fin 0 to 2*nc
	buckets := map[int][]*DiffOfChild{}
	sortedChildren := make([]*DiffOfChild, 0, len(dd))
	for _, info := range dd {
		if _, ok := buckets[int(info.diff)+2*nc]; ok {
			buckets[int(info.diff)+2*nc] = append(buckets[int(info.diff)+2*nc], info)

		} else {
			buckets[int(info.diff)+2*nc] = []*DiffOfChild{info}
		}

	}
	for i := 4 * nc; i >= 0; i-- {
		if val, ok := buckets[i]; ok {
			sort.Slice(val, func(i, j int) bool {
				return val[i].finishedNum < val[j].finishedNum
			})
			sortedChildren = append(sortedChildren, val...)
		}
	}
	return sortedChildren
}

// func infoComparator(a, b interface{}) int {
// 	aAsserted := a.(DiffOfChild)
// 	bAsserted := b.(DiffOfChild)
// 	switch {
// 	case aAsserted.diff < bAsserted.diff:
// 		return 1
// 	case aAsserted.diff > bAsserted.diff:
// 		return -1
// 	default:
// 		switch {
// 		case aAsserted.finishedNum > bAsserted.finishedNum:
// 			return 1
// 		case aAsserted.finishedNum < bAsserted.finishedNum:
// 			return -1
// 		default:
// 			return 0
// 		}
// 	}
// }

func NewExchangeInfo() *ExchangeInfo {
	inner := make(map[*supervisor.Peer]DiffOfChild)
	info := ExchangeInfo{derivation: inner}
	return &info
}

func (info *ExchangeInfo) Insert(d DiffOfChild) {
	info.derivation[d.peer] = d
}

func (info *ExchangeInfo) Delete(p *supervisor.Peer) {
	delete(info.derivation, p)
}

func (info *ExchangeInfo) FindExchange(target *supervisor.Peer) *supervisor.Peer {
	if len(info.derivation) == 0 {
		return nil
	}
	vals := make([]*DiffOfChild, 0, len(info.derivation))
	for _, v := range info.derivation {
		vals = append(vals, &v)
	}
	nc := vals[0].peer.Task.PieceTotal
	vals = bucketSort(vals, int(nc))

	for _, touch := range vals {
		if touch.peer.IsLeave() || touch.peer.IsBad() {
			info.Delete(touch.peer)
		}
		delta := touch.peer.GetParent().GetFinishedNum() - target.GetFinishedNum()
		if delta > 0 && delta < touch.diff {
			return touch.peer
		}
	}
	return nil
}
