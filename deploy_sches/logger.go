package deploy_sches

import (
	"fmt"
	"log"
)

var logger SugaredLoggerOnWith

func init() {
	logger = SugaredLoggerOnWith{}
}

type SugaredLoggerOnWith struct {
	withArgs []interface{}
}

func WithTaskAndPeerID(taskID string, peerID string) *SugaredLoggerOnWith {
	return &SugaredLoggerOnWith{
		withArgs: []interface{}{"taskId", taskID, "peerID", peerID},
	}
}

func (_ *SugaredLoggerOnWith) Infof(template string, args ...interface{}) {
	log.Println("info:{:s}", fmt.Sprintf(template, args...))
}

func (_ *SugaredLoggerOnWith) Info(args ...interface{}) {
	log.Println("info:{:s}", args)
}

func (_ *SugaredLoggerOnWith) Warnf(template string, args ...interface{}) {
	log.Println("warn:{:s}", fmt.Sprintf(template, args...))
}

func (_ *SugaredLoggerOnWith) Warn(args ...interface{}) {
	log.Println("warn:{:s}", args)
}

func (_ *SugaredLoggerOnWith) Errorf(template string, args ...interface{}) {
	log.Println("error:{:s}", fmt.Sprintf(template, args...))
}

func (_ *SugaredLoggerOnWith) Error(args ...interface{}) {
	log.Println("error:{:s}", args)
}

func (_ *SugaredLoggerOnWith) Debugf(template string, args ...interface{}) {
	log.Println("debug:{:s}", fmt.Sprintf(template, args...))
}

func (_ *SugaredLoggerOnWith) Debug(args ...interface{}) {
	log.Println("debug:{:s}", args)
}

func Infof(template string, args ...interface{}) {
	log.Println("info:{:s}", fmt.Sprintf(template, args...))
}

func Info(args ...interface{}) {
	log.Println("info:{:s}", args)
}

func Warnf(template string, args ...interface{}) {
	log.Println("warn:{:s}", fmt.Sprintf(template, args...))
}

func Warn(args ...interface{}) {
	log.Println("warn:{:s}", args)
}

func Errorf(template string, args ...interface{}) {
	log.Println("error:{:s}", fmt.Sprintf(template, args...))
}

func Error(args ...interface{}) {
	log.Println("error:{:s}", args)
}

func Debugf(template string, args ...interface{}) {
	log.Println("debug:{:s}", fmt.Sprintf(template, args...))
}

func Debug(args ...interface{}) {
	log.Println("debug:{:s}", args)
}
