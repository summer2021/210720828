package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"

	"multiply/utils"
)

func InitialSource(storageDir string, serverOpt utils.ServerOptionMultiply, files []int) *http.Server {
	// 写入内容
	err := os.MkdirAll(storageDir, 0775)
	if err != nil {
		log.Panicf("make dir error: %s", err)
	}
	for i, fileSize := range files {
		dst := path.Join(storageDir, fmt.Sprint(i))
		println(dst)
		file, err := os.OpenFile(dst, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0664)
		if err != nil {
			log.Panicf("file open error: %s", err)
		}
		write := bufio.NewWriter(file)
		piece := utils.RandomString(fileSize)
		write.Write([]byte(piece))
		write.Flush()
		file.Close()
	}
	// 启动本地服务器
	addr := fmt.Sprintf("%s:%d", serverOpt.IP, serverOpt.Port)
	fmt.Print(addr)

	handler := http.FileServer(http.Dir(storageDir))

	srv := &http.Server{Addr: addr, Handler: handler}

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			log.Panicf("create source server error: %s", err)
		}
	}()

	return srv
}

func main() {
	// 启动钩子
	InitialHook()
	// 读取配置
	configFile := path.Join(utils.ConfigDir, "config.yaml")
	cfg := utils.MultiplyConfig{}
	utils.LoadConfig(configFile, &cfg)
	// 创建本地下载源
	srv := InitialSource(utils.SourceDir, cfg.Source, cfg.Files)
	log.Println("source server up")
	defer srv.Close()

	utils.WriteForFinish("source", 3)
	log.Printf("write seq file source")

	utils.WaitForFinish("download")
	log.Printf("server closed")
}
