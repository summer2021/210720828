package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path"

	"d7y.io/dragonfly/v2/cdnsystem"
	"d7y.io/dragonfly/v2/cdnsystem/config"
	_ "d7y.io/dragonfly/v2/pkg/source/httpprotocol"

	"multiply/utils"
)

func InitialCDN(CDNOpt utils.CDNMultiply) func() {
	diskPath := path.Join(utils.CDNDir, "disk")
	memPath := path.Join(utils.CDNDir, "mem")
	err := os.MkdirAll(diskPath, 0775)
	if err != nil {
		log.Panicf("make dir error: %s", err)
	}
	err = os.MkdirAll(memPath, 0775)
	if err != nil {
		log.Panicf("make dir error: %s", err)
	}
	cfg := &config.Config{
		BaseProperties: config.NewDefaultBaseProperties(),
		Plugins:        NewPlugins(diskPath, memPath),
	}
	cfg.BaseProperties.ListenPort = CDNOpt.ListenPort
	cfg.BaseProperties.DownloadPort = CDNOpt.DownloadPort
	cfg.BaseProperties.AdvertiseIP = CDNOpt.IP

	// create server
	CDN, err := cdnsystem.New(cfg)
	log.Printf("init cdn download port:%d listen port:%d", cfg.DownloadPort, cfg.ListenPort)
	if err != nil {
		log.Panicf("create server error: %s", err)
	}

	// start server
	go CDN.Serve()
	// start file server
	addr := fmt.Sprintf("%s:%d", CDNOpt.IP, CDNOpt.DownloadPort)
	fmt.Print(addr)

	handler := http.FileServer(http.Dir(diskPath))

	srv := &http.Server{Addr: addr, Handler: handler}

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			log.Panicf("create cdn download server error: %s", err)
		}
	}()
	// forbidden auto choose port
	return func() {
		srv.Close()
	}
}

func main() {
	// 读取配置
	configFile := path.Join(utils.ConfigDir, "config.yaml")
	cfg := utils.MultiplyConfig{}
	utils.LoadConfig(configFile, &cfg)
	// 启动钩子
	InitialHook()
	// 创建scheduler
	closer := InitialCDN(cfg.CDN)
	defer closer()

	utils.WriteForFinish("cdn", 3)
	log.Printf("write seq file sche")

	utils.WaitForFinish("download")
	log.Printf("server closed")
}
