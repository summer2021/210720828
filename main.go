package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/otiai10/copy"

	"d7y.io/dragonfly/v2/manager/model"
	"d7y.io/dragonfly/v2/pkg/util/net/iputils"

	"multiply/utils"
)

var tmpDirs = [...]string{utils.DaemonDir, utils.SourceDir, utils.DownloadDir, utils.FlowDir, utils.ConfigDir, utils.CDNDir}

func clearEnv() {
	for _, file := range tmpDirs {
		err := os.RemoveAll(file)
		if err != nil {
			log.Panicf("make dir error: %s", err)
		}
	}
}

func makeEnv(testcase utils.MultiplyConfig, sche string) utils.MultiplyConfig {
	for _, file := range tmpDirs {
		err := os.MkdirAll(file, 0775)
		if err != nil {
			log.Panicf("make dir error: %s", err)
		}
	}

	copy.Copy(utils.ConfigTemplate, utils.ConfigDir)

	// Auto chose port config and write ports to testcase
	configFile := path.Join(utils.ConfigDir, "config.yaml")
	testcase = utils.OverwriteConfig(testcase, configFile, sche)

	// Overwrite cdn config
	cfg := &model.CDN{}
	bytes, err := ioutil.ReadFile(path.Join(utils.ConfigDir, "cdn", "manager.json"))
	err = json.Unmarshal(bytes, &cfg)
	if err != nil {
		log.Panicf("read cdn config error: %s", err)
	}
	cfg.IP = testcase.CDN.IP
	cfg.Port = int32(testcase.CDN.ListenPort)
	cfg.DownloadPort = int32(testcase.CDN.DownloadPort)
	cfg.HostName = iputils.HostName

	bytes, err = json.Marshal(cfg)
	ioutil.WriteFile(path.Join(utils.ConfigDir, "cdn", "manager.json"), bytes, 0664)

	return testcase
}

func seqExec(last string, now string, dir string) (func(), func()) {

	var outfile, errfile *os.File
	var err error
	var cmd *exec.Cmd

	err = os.MkdirAll(dir, 0775)
	if err != nil {
		log.Panicf("make dir error: %s", err)
	}

	if last != "" {
		utils.WaitForFinish(last)
	}

	cmd = exec.Command(path.Join(utils.BinDir, now))
	cmd.Dir = dir
	outfile, err = os.Create(path.Join(dir, fmt.Sprintf("%s.out", now)))
	if err != nil {
		panic(err)
	}
	errfile, err = os.Create(path.Join(dir, fmt.Sprintf("%s.err", now)))
	if err != nil {
		panic(err)
	}
	cmd.Stdout = outfile
	cmd.Stderr = errfile
	err = cmd.Start()
	if err != nil {
		log.Panicf("run cmd error: %s", err)
	}
	return func() {
			outfile.Close()
			errfile.Close()
		}, func() {
			cmd.Wait()
		}
}

func seqExecTimes(last string, now string, times int, dir string) (func(), func()) {
	var err error

	err = os.MkdirAll(dir, 0775)
	if err != nil {
		log.Panicf("make dir error: %s", err)
	}

	if last != "" {
		utils.WaitForFinish(last)
	}
	opened := []*os.File{}
	cmds := []*exec.Cmd{}
	var cmd *exec.Cmd
	for i := 0; i < times; i++ {
		addup := fmt.Sprintf("-index=%d", i)
		cmd = exec.Command(path.Join(utils.BinDir, now), addup)
		cmd.Dir = dir
		outfile, err := os.Create(path.Join(dir, fmt.Sprintf("%s_%d.out", now, i)))
		if err != nil {
			panic(err)
		}
		errfile, err := os.Create(path.Join(dir, fmt.Sprintf("%s_%d.err", now, i)))
		if err != nil {
			panic(err)
		}

		cmd.Stdout = outfile
		cmd.Stderr = errfile
		opened = append(opened, outfile)
		opened = append(opened, errfile)
		cmds = append(cmds, cmd)
		err = cmd.Start()
		if err != nil {
			log.Panicf("run cmd error: %s", err)
		}
	}
	for i := 0; i < times; i++ {
		utils.WaitForFinish(fmt.Sprintf("daemon_%d", i))
	}
	utils.WriteForFinish("daemon", 3)
	return func() {
			for _, file := range opened {
				file.Close()
			}
		}, func() {
			for _, cmd := range cmds {
				cmd.Wait()
			}
		}
}

func main() {
	go flag.Parse()

	// 读取测试环境配置
	allCfg := make(map[string]utils.MultiplyConfig)
	testCaseDir := path.Join(utils.TestcaseDir, "environments")
	envs, _ := ioutil.ReadDir(testCaseDir)
	log.Printf("register %d test cases\n", len(envs))
	for _, env := range envs {
		configFile := path.Join(testCaseDir, env.Name())
		cfg := utils.MultiplyConfig{}
		utils.LoadConfig(configFile, &cfg)
		inx := strings.LastIndex(env.Name(), ".")
		allCfg[env.Name()[:inx]] = cfg
	}

	// 读取调度策略配置
	workFile := path.Join(utils.TestcaseDir, "workflows.yaml")
	workflows := utils.WorkFLows{}
	utils.LoadConfig(workFile, &workflows)

	envNames := []string{}

	for _, env := range workflows.Envs {

		envNames = append(envNames, env)

		for _, sche := range workflows.Sches {
			workName := utils.GetWorkName(env, sche)
			os.RemoveAll(workName)
			log.Printf("begin workflow for sche %s and env %s\n", sche, env)

			clearEnv()

			testcase := allCfg[env]
			testcase = makeEnv(testcase, sche)

			sourceCloser, sourceWaiter := seqExec("", "source", workName)
			defer sourceCloser()

			scheCloser, scheWaiter := seqExec("source", "cdn", workName)
			defer scheCloser()

			CDNCloser, CDNWaiter := seqExec("cdn", "sche", workName)
			defer CDNCloser()

			daemonCloser, daemonWaiter := seqExecTimes("sche", "daemon", len(testcase.Daemons), workName)
			defer daemonCloser()

			downloadCloser, downloadWaiter := seqExec("daemon", "download", workName)
			defer downloadCloser()

			downloadWaiter()
			sourceWaiter()
			scheWaiter()
			CDNWaiter()
			daemonWaiter()

			clearEnv()
		}
	}
	// 运行时间数据写入报表
	utils.TableAllStatus(envNames, workflows.Sches, false)
}
