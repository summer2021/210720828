subModules=("source" "cdn" "sche" "daemon" "download")
execPath=$(readlink -f "$(dirname "$0")")

for MODULE in ${subModules[*]}; 
do
     echo "compile ${MODULE}..."
     go build -gcflags "-N -l"  -o ${execPath}/bin/${MODULE} ${execPath}/mod_${MODULE}/*
done
echo "compile main..."
go build -gcflags "-N -l"  -o ${execPath}/bin/main ${execPath}/main.go

chmod 755 ${execPath}/bin/*