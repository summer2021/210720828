package utils

import (
	"bytes"
	"fmt"
	"log"
	"path"
	"sort"

	"github.com/olekukonko/tablewriter"
)

type Statistics struct {
	Schedulers     string `yaml:"sche,omitempty"`
	Workflows      string `yaml:"work,omitempty"`
	MinScheWork    int    `yaml:"min"`
	MedianScheWork int    `yaml:"median"`
	MeanScheWork   int    `yaml:"mean"`
	MaxScheWork    int    `yaml:"max"`
	Times          []int  `yaml:"costs"`
}

func SaveStatus(sta Statistics) {
	SaveConfig(sta, "cost.yaml")
}

func GetStatus(seqTime []int) (sta Statistics) {
	mean := 0
	for _, v := range seqTime {
		mean += int(v)
	}
	mean /= len(seqTime)
	sort.Slice(seqTime, func(i, j int) bool {
		return seqTime[i] < seqTime[j]
	})
	min := seqTime[0]
	max := seqTime[len(seqTime)-1]
	median := seqTime[int(len(seqTime)/2)]

	sta = Statistics{MinScheWork: min, MedianScheWork: median, MeanScheWork: mean, MaxScheWork: max, Times: seqTime}
	return
}

func TableAllStatus(envs []string, sches []string, removeOutlier bool) {
	// add comment for 2d table left-top
	headers := append(sches, "")
	copy(headers[1:], sches)
	headers[0] = "min/median/mean/max(ms)"

	sches = append(sches)
	buffer := bytes.NewBuffer([]byte{})
	table := tablewriter.NewWriter(buffer)
	table.SetHeader(headers)

	for _, env := range envs {
		tableLine := []string{}
		for _, sche := range sches {
			workName := GetWorkName(env, sche)
			sta := Statistics{}
			LoadConfig(path.Join(workName, "cost.yaml"), &sta)
			if removeOutlier == false {
				tableLine = append(tableLine, fmt.Sprintf("%d/%d/%d/%d",
					sta.MinScheWork, sta.MedianScheWork, sta.MeanScheWork, sta.MaxScheWork))
			} else {
				checkSeq := sta.Times
				// reverse iter check last abormal data
				split := len(checkSeq)
				for i := len(checkSeq) - 1; i >= 0; i-- {
					if checkSeq[i] > 3*sta.MedianScheWork {
						log.Printf("warn: drop abormal time %dms(> 3 * median %dms) for sche %s and env %s",
							checkSeq[i], sta.MedianScheWork, sche, env)
						split = i
					} else {
						checkSeq = checkSeq[:split]
						break
					}
				}
				sta := GetStatus(checkSeq)
				tableLine = append(tableLine, fmt.Sprintf("%d/%d/%d/%d",
					sta.MinScheWork, sta.MedianScheWork, sta.MeanScheWork, sta.MaxScheWork))
			}
		}
		tableLine = append(tableLine, "")
		copy(tableLine[1:], tableLine)
		tableLine[0] = env
		table.Append(tableLine)
	}
	table.Render()
	fmt.Println(buffer.String())

}
