package utils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path"
	"time"

	"github.com/phayes/freeport"
	"gopkg.in/yaml.v2"
)

type DaemonOptionMultiply struct {
	IP      string `yaml:"ip"`
	RPCPort int    `yaml:"rpc-port"`
	UpPort  int    `yaml:"up-port"`
}

type TransportStatus struct {
	start  int
	end    int
	length uint64
}

type ServerOptionMultiply struct {
	Name string `yaml:"name,omitempty"`
	IP   string `yaml:"ip"`
	Port int    `yaml:"port"`
}

type CDNMultiply struct {
	IP           string `yaml:"ip"`
	ListenPort   int    `yaml:"listen-port"`
	DownloadPort int    `yaml:"down-port"`
}

type MultiplyConfig struct {
	Files        []int                  `yaml:"files"`
	Source       ServerOptionMultiply   `yaml:"source"`
	CDN          CDNMultiply            `yaml:"cdn"`
	Scheduler    ServerOptionMultiply   `yaml:"scheduler"`
	DaemonsCount int                    `yaml:"daemons_num"`
	Daemons      []DaemonOptionMultiply `yaml:"daemons,omitempty"`
	Gets         []int                  `yaml:"gets"`
	GetsFileSeq  [][]int                `yaml:"gets_seq,omitempty"`
}

type WorkFLows struct {
	Sches []string `yaml:"sches"`
	Envs  []string `yaml:"envs"`
}

func GetWorkName(env string, sche string) string {
	return fmt.Sprintf("%s__%s", env, sche)
}

func RandomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}

func LoadConfig(filename string, out interface{}) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Panicf("read test config %s error: %s", filename, err)
	}
	err = yaml.Unmarshal(bytes, out)
	if err != nil {
		log.Panicf("load test config %s error: %s", filename, err)
	}
}

func SaveConfig(in interface{}, filename string) {
	bytes, err := yaml.Marshal(in)
	if err != nil {
		log.Panicf("load test config error: %s", err)
	}
	err = ioutil.WriteFile(filename, bytes, 0664)
	if err != nil {
		log.Panicf("read test config error: %s", err)
	}
}

func OverwriteConfig(testcase MultiplyConfig, filename string, sche string) MultiplyConfig {
	ports, err := freeport.GetFreePorts(testcase.DaemonsCount*2 + 4)
	// log.Printf("use ports: %v\n", ports)

	if err != nil {
		log.Panicf("fetach free ports error: %s", err)
	}

	testcase.Source.Port = ports[0]

	testcase.CDN.DownloadPort = ports[1]
	testcase.CDN.ListenPort = ports[2]

	testcase.Scheduler.Port = ports[3]
	testcase.Scheduler.Name = sche

	for i := 0; i < testcase.DaemonsCount; i++ {
		d := DaemonOptionMultiply{}
		d.IP = "127.0.0.1"
		d.RPCPort = ports[i*2+4]
		d.UpPort = ports[i*2+5]
		testcase.Daemons = append(testcase.Daemons, d)
		testcase.GetsFileSeq = append(testcase.GetsFileSeq, testcase.Gets)
	}

	SaveConfig(testcase, filename)
	return testcase
}

func WriteForFinish(flag string, wait uint) {
	time.Sleep(time.Second * time.Duration(wait))
	dst := path.Join(FlowDir, flag)
	file, err := os.OpenFile(dst, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0664)
	if err != nil {
		log.Printf("write to seq file error: %s", err)
		panic(err)
	}
	file.Close()
}

func WaitForFinish(flag string) {
	dst := path.Join(FlowDir, flag)
	interval := 0
	for {
		if interval >= TimeExceed {
			err := errors.New("too long for file to generate")
			log.Printf("%s", err)
			panic(err)
		}
		_, err := os.Stat(dst)
		if err != nil {
			if os.IsNotExist(err) {
				interval += 1
				time.Sleep(time.Second * 1)
				continue
			} else {
				log.Printf("file status error: %s", err)
				panic(err)
			}
		}
		break
	}
}

func CheckTuple(a interface{}, b interface{}) {
	if a != b {
		err := errors.New("not equal")
		log.Printf("%s:%v and %v", err, a, b)
		panic(err)
	}
}
