package utils

import (
	"os"
	"path"
	"path/filepath"
	"runtime"
)

var (
	TmpDir      string
	DaemonDir   string
	SourceDir   string
	DownloadDir string
	FlowDir     string
	CDNDir      string
	TimeExceed  int

	ProjectRoot    string
	BinDir         string
	ConfigDir      string
	TestcaseDir    string
	ConfigTemplate string
)

func init() {
	TmpDir, err := os.UserHomeDir()
	if err != nil {
		TmpDir = os.TempDir()
	}

	DaemonDir = path.Join(TmpDir, "/.dragonfly/daemon/")
	SourceDir = path.Join(TmpDir, "/.dragonfly/source/")
	DownloadDir = path.Join(TmpDir, "/.dragonfly/download/")
	FlowDir = path.Join(TmpDir, "/.dragonfly/flow/")
	ConfigDir = path.Join(TmpDir, "/.dragonfly/config/")
	CDNDir = path.Join(TmpDir, "/.dragonfly/cdn/")

	_, b, _, _ := runtime.Caller(0)
	ProjectRoot = path.Join(filepath.Dir(b), "..")
	BinDir = path.Join(ProjectRoot, "bin")
	ConfigTemplate = path.Join(ProjectRoot, "config")

	TestcaseDir = path.Join(ProjectRoot, "testcase")
	TimeExceed = 1200
}
